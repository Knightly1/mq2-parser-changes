![currentstatus] MQ2 Parser Changes 
========================

  # Overview
  
  Updates the MQ2 Parser to allow parsing by variable individually by implementing a per-variable parser override
  as `${Parse[<iterations>,<thingtoparse>]}`
  
  As a demonstration of this functionality, implements `#bind_noparse` that allows a macro writer to create a command
  that accepts unparsed input from the user without the user needing to have any knowledge of how the parser functions.
  The macro writer can then parse that as needed (or continue to pass it around unparsed).

  ## Documentation

  A full explanation of the entire parsing process can be found in the [color-coded guide][documentation].

  To use the Parse Override functionality, wrap your variable (or any string) in the Parse and the number of iterations.
  For example, if you wanted to show ${Me.Name} you could `/echo ${Parse[0,${Me.Name}]}` and the output would be
  unparsed.

  ### INI Settings

  Adds:

  ```ini
  [MacroQuest]
  Parserengine=1
  ```

  To set the default value of the parser by user preference.  Valid options are:
  - 1 : The original parser.  `${Parse[]}` parameters will not work.
  - 2 :  The new parser.  `${Parse[]}` parameters will work.  See [issues] for any identified issues.

  ### TLOs

  Adds the TLO `${MacroQuest.Parser}` to check the parser version.  Returns an integer.

  Example:

  ```cpp
  /echo The Parser is Version ${MacroQuest.Parser}
  ```

  Would output the below by default:

  ```cpp
  [MQ2] The Parser is Version 1
  ```

  ### Slash Commands

  Adds the slash command `/engine parser <version> [noauto]` to allow you to change the parser either as default (which
  will set it in the ini) or using `noauto` which will not set it in the ini.

  Note that regardless of the setting here, when a macro ends the parser will revert back to the default that is in the
  ini (or 1 if no setting in the ini).

  `noauto` is designed to allow you to change the parser, perform an action, and then change the parser back.

  Example:
  
  ```cpp
  /engine parser 2 noauto
  /echo ${Parse[0,${Me.Name}]} is Knightly
  /engine parser 1 noauto
  ```

  The above will leave the user's default settings, but change the parser to version 2, perform the echo, and then
  change the parser back to version 1.  This will output:

  ```cpp
  [MQ2] ${Me.Name} is Knightly
  ```

  Note that the parser change is still global, so during the time that the `/echo` above is processing, anything that
  passes through the parser is passing through the version 2 parser.

  ### Macro Directives

  Adds the directive `#engine parser <version>` to allow you to change the parser while your macro is running.  Note that
  the parser change is still a global change and will impact anything that gets parsed.

  When your macro ends, the parser version will be reset to whatever the user has set in their ini (or 1 by default).

  Example:  `#engine parser 2`
  
  ## What Else
  
  - Mostly backwards compatible so existing macros should continue to run
  - Replaces global variables for parsing allowing for future concurrency
  - Resolves some minor bugs in the current parser functionality
  - May cause issues with macros that have "{" or "}" in them not expecting to be matched (but I couldn't find 
    any that did this)
  - This takes some liberties with parser changes, so it needs to be tested more with existing macros
  - /noparse works for backwards compatibility, but instead of `/noparse /echo ${Me.Name}` you should use 
    `/echo ${Parse[0,${Me.Name}]}`
  - The files in this repository are just the changed files (ie, it will not compile on its own)
  - Current as of MQ2-20190821(TEST).zip
  
  ## Example Bind Test

  Below is a test macro demonstrating how to use the new functionality.  To see the difference:
  1. Save the below macro as [TestParseBinds.mac]
  2. Run `/mac testparse`
  3. Type `/testone ${Me.Name}`
  4. Note the output is your character name
  5. Type `/testtwo ${Me.Name}`
  6. Note the output is ${Me.Name}
  
```cpp
| Enable the new parsing engine
#engine parser 2

| /testone is normal bind functionality
#bind TestABind     /testone

| /testtwo is no parse functionality
#bind_noparse TestABindTwo  /testtwo

Sub Main
    /if (${MacroQuest.Parser} == 2) {
        /declare DummyDoEvents string local
        /echo Starting Test...
        /while (1) {
            /doevents
            /varset DummyDoEvents "Test"
            /delay 1s
        }
    } else {
        /echo Parser Version 2 not Enabled.  Cannot continue.
    }
/return

Sub Bind_TestABind(Param1)
    /if (!${Param1.Length}) {
        /echo No parameter found
    } else {
        /echo ${Param1}
    }
/return

Sub Bind_NoParse_TestABindTwo(Param1)
    /if (!${Param1.Length}) {
        /echo No parameter found
    } else {
        /echo ${Parse[1,${Param1}]}
    }
/return
```
  
  [currentstatus]: https://img.shields.io/badge/Status-Released-green.svg
  [documentation]: https://gitlab.com/Knightly1/mq2-parser-changes/raw/master/MQ2_Parser_Docs.pdf?inline=false
  [issues]: https://gitlab.com/Knightly1/mq2-parser-changes/issues
  [TestParseBinds.mac]: https://gitlab.com/Knightly1/mq2-parser-changes/raw/master/TestParseBinds.mac?inline=false
  